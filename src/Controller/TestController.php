<?php

declare(strict_types=1);

namespace App\Controller;


use Gocool\VehicleDealerBundle\Services\Engine;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route(path="/test-bundle")
     */
    public function testbundle(Engine $engine): Response
    {
        return new Response($engine->applyTurboSpeed(20));
    }
}