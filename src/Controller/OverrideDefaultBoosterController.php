<?php

declare(strict_types=1);

namespace App\Controller;


use Gocool\VehicleDealerBundle\Services\Engine;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OverrideDefaultBoosterController
{
    /**
     * @Route(path="/override-default-bundle")
     */
    public function overrideDefaultbundle(Engine $engine): Response
    {
        return new Response($engine->applyTurboSpeed(20));
    }
}