<?php

declare(strict_types=1);

namespace App\Services;

use Gocool\VehicleDealerBundle\Services\BoosterProvider;

class MyNitrogenBooster implements BoosterProvider
{
    public function addBooster(): int
    {
        return 100;
    }
}